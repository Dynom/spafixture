<?php

namespace Speakap;

use Symfony\Component\Console\Command\Command;

use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;



/**
 *  Network                 ID
 *  ----------------------------
 *  loada.speakap.nl        1161
 *  loadb.speakap.nl        1162
 *  loadc.speakap.nl        1163
 *  loadd.speakap.nl        1164
 *
 *
 *
 * @author Mark van der Velden <mark@dynom.nl>
 */
class FixtureLoadCommand extends Command
{
    /**
     * @var \Guzzle\Batch\BatchBuilder
     */
    private $batchBuilder;

    /**
     * @var \Guzzle\Http\Client
     */
    private $httpClient;


    /**
     *
     */
    protected function configure()
    {
        $this
            ->setName('loader:run')
            ->setDescription('Run the importer')
            ->addArgument(
                'csv-file',
                InputArgument::REQUIRED,
                'Path to the CSV file with user data'
            )
        ;
    }


    /**
     * Servers to handle: load[abcd].speakap.nl
     *
     *
     * @param \Symfony\Component\Console\Input\InputInterface $input
     * @param \Symfony\Component\Console\Output\OutputInterface $output
     *
     * @return int|null|void
     * @throws \InvalidArgumentException
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $filePath = $input->getArgument('csv-file');

        if ( ! file_exists($filePath)) {
            throw new \InvalidArgumentException('Unable to find the file "'. $filePath .'"');
        }

        if ( ! is_readable($filePath)) {
            throw new \InvalidArgumentException('File exists, but it\'s not readable.');
        }


        // Locate and open the file.
        $file = new \SplFileInfo($filePath);
        $fileHandle = $file->openFile('r');

        // The networks we're serving, the syntax is Identifier => name
        $networks = array(
            1161 => 'loada',
            1162 => 'loadb',
            1163 => 'loadc',
            1164 => 'loadd'
        );

        // Used in our random algorithm, gives a little more weight to one server or the other
        $networkIds = array(
            1161,
            1162, 1162,
            1163, 1163,
            1164, 1164, 1164, 1164
        );


        // For each line in our CSV file
        while ( ! $fileHandle->eof()) {

            // Reading the line
            $data = $fileHandle->fgetcsv(';', '"');


            // Picking a random network
            $networkIdIndex = array_rand(
                $networkIds,
                1
            );

            $networkId = $networkIds[ $networkIdIndex ];

            // Creating a workable data-structure for our wrappers
            $data['network_data'] = array(
                'id' => $networkId,
                'name' => $networks[ $networkId ]
            );

            // Create the two payload's
            $payLoadAdd = $this->createAddUserJSON($data);
            $payLoadActivate = $this->createActivateUserJSON($data);

            // Debug
//            $output->writeln($payLoadAdd);
//            $output->writeln($payLoadActivate);
            $output->write('.');

            // Queue the requests
            if ($this->queueRequestFromData($payLoadAdd, $data)) {
                if ( ! $this->queueRequestFromData($payLoadActivate, $data)) {
                    $output->writeln('JSON errors, for the following payload:');
                    $output->writeln($payLoadActivate);
                }
            } else {
                $output->writeln('JSON errors, for the following payload:');
                $output->writeln($payLoadAdd);
            }

        }
    }


    /**
     * Create and queue a request, based on the JSON payload
     *
     * @param string $payload
     * @param array $data
     *
     * @return bool
     */
    protected function queueRequestFromData($payload, array $data)
    {
        // Testing JSON payload
        $payload = json_encode(json_decode($payload));

        if (json_last_error()) {
            return false;
        }


        $url = 'http://'. $data['network_data']['name'] .'.speakap.nl/jsonfunction.php';
        $this->handleRequest(
            $this->getHttpClient()->post(
                $url,
                array('Content-Type: application/json'),
                'JSON=['. $payload .']'
            )
        );

        return true;
    }


    /**
     * Return the instance of a prepared Guzzle HTTP client
     *
     * @return \Guzzle\Http\Client
     */
    private function getHttpClient()
    {
        if ( ! ($this->httpClient instanceof \Guzzle\Http\Client)) {

            $httpClient = new \Guzzle\Http\Client();

            // Adding a logger
//            $httpClient->addSubscriber(
//                new \Guzzle\Plugin\Log\LogPlugin(
//                    new \Guzzle\Log\MonologLogAdapter(
//                        new \Monolog\Logger('fl')
//                    )
//                )
//            );

            // Make sure the server things we're an AJAX request
            $httpClient->setDefaultHeaders(array('X-Requested-With: XMLHttpRequest'));

            $this->httpClient = $httpClient;
        }

        return $this->httpClient;
    }


    /**
     * @param \Guzzle\Http\Message\RequestInterface $request
     */
    protected function handleRequest(\Guzzle\Http\Message\RequestInterface $request)
    {
        if ( ! ($this->batchBuilder instanceof \Guzzle\Batch\BatchInterface)) {
            $this->batchBuilder = \Guzzle\Batch\BatchBuilder::factory()
                ->transferRequests(9)
                ->autoFlushAt(3)
                ->bufferExceptions()
                ->build();
        }

        $this->batchBuilder->add($request);
    }


    /**
     * Wrapping the data in a JSON blob matching the following:
     *
     * @see {Model : "User", Function : "Add_User", Params : { User_EMail : "*User_EMail*", Network_ID : *Network_ID*}};
     *
     * @param array $data
     *
     * @return string
     */
    protected function createAddUserJSON(array $data)
    {
        return <<<"EOJSON"
{
    "Model" : "User",
    "Function" : "Add_User",
    "Params" : {
        "User_EMail" : "{$data[1]}",
        "Network_ID" : "{$data['network_data']['id']}"
    }
}
EOJSON;
    }


    /**
     * Wrapping the data in a JSON blob matching the following:
     *
     * @see {Model : "User", Function : "Activate_User", Params : { User_ID : *User_ID*, PasswordConfirm : "test", Data : { Password : "test", FirstName : "*FirstName*", LastName : "*LastName*", "Gender_ID" : 1 } } };
     *
     * @param array $data
     *
     * @return string
     */
    protected function createActivateUserJSON(array $data)
    {
        return <<<"EOJSON"
{
    "Model" : "User",
    "Function" : "Activate_User",
    "Params" : {
        "User_ID" : {$data[0]},
        "PasswordConfirm" : "test",
        "Data" : [{
            "Password" : "test",
            "FirstName" : "{$data[2]}",
            "LastName" : "{$data[3]}",
            "Gender_ID" : 1
        }]
    }
}
EOJSON;

    }
}
