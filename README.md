Installation:
=============

    ./composer.phar install


Execute as:
============

    ./app/console loader:run dataset.csv


Networks:
=========

    *  Network    ID
    *  ---------------
    *  loada      1161
    *  loadb      1162
    *  loadc      1163
    *  loadd      1164


Dependencies:
=============

* PHP >=5.3.7
* Composer -- http://getcomposer.org/download/
